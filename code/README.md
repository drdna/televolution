**Instructions for Running the Various Telomere Feature Scripts**

**Internal_tel_finder.py**

1. Place genomes (with .fasta suffixes) in a directory called TelFinder_genomes

2. If desired generate a .csv file of chromosome regions to exclude from final report (e.g. internal telomeres caused by telomeric transposon insertions). File format should be Strain,Chromosome,Pos_start+50, Pos_end-50, where Pos-start+50 is the position from which to start searching on the chromosome and Pos_end-50 is the position to stop searching. See Excluded_positions.csv for an example

3. Run script:

`python Internal_tel_finder.py`

**Fig1C.R**

1. Uses the TJ_Final.csv as input. Make sure the file is in the home directory, or modify the code to specify the new directory path.

2. Run the Fig1C.R from RStudio.

**Fig1D.R**

1. Uses the TAS_BLAST_Mastersheet_Final.csv and Random_Sampling280bp_Final.csv as inputs. Make sure these files are in the home directory, or modify the code to specify the new directory paths.

2. Run the Fig1D.R from RStudio.

**TAS_IT_Plotter_color_50kb_MR.R**

1. Uses the TrimmedConsensus.CD156.BLASTunique_Redundant_removed_MR2.txt and All_internal_Tels_50bpGap_relative.csv files as inputs. Make sure these files are in the home directory, or modify the code to specify the new directory paths.

2. Run the TAS_IT_Plotter_color_50kb_MR.R code from RStudio.
