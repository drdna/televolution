**Data and Code for Identifying and Plotting Telomere-Related Sequence Features**

These resources can be used to recreate figures presented in Rahnama et al. (submitted) "Telomere Roles in Fungal Genome Evolution and Adaptation." Frontiers in Genetics special issue on "Telomere Flexibility and Versatility: a Role of Telomeres in Adaptive Potential"
